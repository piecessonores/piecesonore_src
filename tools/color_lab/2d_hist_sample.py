import numpy as np
import cv2
from time import clock
import sys

shift = 50

def shift_hue(mat):
    return np.append(mat[shift:], mat[0:shift], 0)

if __name__ == '__main__':

    hsv_map = np.zeros((180, 256, 3), np.uint8)
    h, s = np.indices(hsv_map.shape[:2])
    hsv_map[:,:,0] = h
    hsv_map[:,:,1] = s
    hsv_map[:,:,2] = 255
    hsv_map = cv2.cvtColor(hsv_map, cv2.COLOR_HSV2BGR)
    hsv_map = shift_hue(hsv_map)
    cv2.imshow('hsv_map', hsv_map)

    cv2.namedWindow('hist', 0)
    hist_scale = 31
    def set_scale(val):
        global hist_scale
        hist_scale = val
    cv2.createTrackbar('scale', 'hist', hist_scale, 64, set_scale)
    dark_threshold = 40
    def set_dt(val):
	global dark_threshold
        dark_threshold = val
    cv2.createTrackbar('dark threshold', 'hist', dark_threshold, 255, set_dt)

    try: fn = sys.argv[1]
    except: fn = 0

    frame = cv2.imread('camera.jpg')
    cv2.imshow('camera', frame)
        
    while True:
        small = cv2.pyrDown(frame)

        hsv = cv2.cvtColor(frame, cv2.COLOR_BGR2HSV)
#	print type(hsv)
        dark = hsv[...,2] < dark_threshold
        hsv[dark] = 0
        h = cv2.calcHist( [hsv], [0, 1], None, [180, 256], [0, 180, 0, 256] )

#	print h.shape

	h = shift_hue(h)

#	print h.shape

        h = np.clip(h*0.005*hist_scale, 0, 1)
        vis = hsv_map*h[:,:,np.newaxis] / 255.0
        cv2.imshow('hist', vis)

        ch = 0xFF & cv2.waitKey(500)
        if ch == 113:
            break

    cv2.destroyAllWindows() 			

