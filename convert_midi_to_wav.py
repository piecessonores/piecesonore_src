import os
import Tkinter as tk
import ttk

# change the midi file name here! Don't add the extension!
# midi_file = "midi85"

def convert(file):
    # don't touch!
    command_prefix = "./tools/timidity ./datas/archives/midi/"
    command_input = file
    command_middle = " -a -c ./tools/timidity.cfg -Ow -o "
    command_output = wav_archive_folder + "/" + file + ".wav"

    command = command_prefix + command_input + command_middle + command_output
    os.system(command)

midi_archive_folder = "datas/archives/midi"
wav_archive_folder = "datas/archives/wav"


def generate_file_list():

    global midi_archive_folder
    files = []

    if not os.path.isdir(midi_archive_folder):
        print("ERROR : no midi archives folder detected")
        print("quit")
        quit()
    else:
        for file in os.listdir(midi_archive_folder):
            if ".mid" in file:
                files.append(file)

        print("files list is : ", files)
        return files


choosed_file = None

def make_choose_file_ui(root, files):

    frame = tk.Frame(root)
    frame.pack()

    button_choose = tk.Button(
        frame, text="Convert this file :",
        command=gui_convert_file_callback
    )
    button_choose.pack(side=tk.LEFT)

    files_menu = ttk.Combobox(frame, values=files, width=13)
    files_menu.current(0)
    files_menu.bind("<<ComboboxSelected>>", get_file_callback)
    files_menu.pack()


def gui_convert_file_callback():

    print("Will convert ", choosed_file)
    convert(choosed_file)


def get_file_callback(event):

    global choosed_file
    choosed_file = event.widget.get()
    print("choosed ", choosed_file)


def main():
    global wav_archive_folder

    files = generate_file_list()

    # prepare wav files folder
    if not os.path.isdir(wav_archive_folder):
        os.mkdir(wav_archive_folder)

    # user interface set-up
    root = tk.Tk()
    root.geometry('300x150')

    make_choose_file_ui(root,files)

    # start UI interaction
    root.mainloop()
    root.destroy()


if __name__ == "__main__":
    # execute only if run as a script
    main()
