# Pièces Sonore

An installation designed to automatically interpret the music of a visual musical score composed on a physical board.

It makes use of computer vision technologies and engeenering associated to graphic design to help with musical langage learning in various education contexts.

## How to run it on desktop (Mac OS X or Linux)

This project has been written in Python. So you need a Python interpreter and you need to be familiar with the terminal.

### Install OpenCV for Python

Read the instructions here : [https://pypi.org/project/opencv-python/](https://pypi.org/project/opencv-python/)

The quick advice is to run :

> pip install opencv-contrib-python

You may need to add a pricise version number to match your Python version and OS environment, like :

> pip install opencv-contrib-python==3.3.0.9

### Install all the dependencies

If you don't already have all of it. Here is all that we use :

numpy, argparse, imutils, os, re, fpt, json, color_tools, pretty_midi, Tkinter, ttk

## How to use

A small UI has been designed to interact with the program.

1. Capture a calibration image, it will be kept in the project folder for further usage.
2. Make a calibration (automatic crop and orientation correction of images)
3. If it fails, color tracking must be adjusted :
  * Pick colors in the image : 1st click for R, 2nd for G, 3rd for B, then press any key to quit. Color tracking will be updated.
  * NB: if anything goes wrong, simply trash the ```colors_config.json``` file, it will reset the tracking to it's default values.
4. Capture an image of the board you want to process.
5. Analyse it.
6. Play it!

Captured files and Midi files are automatically archived.

## How To convert a midi file to a wav file

All generated midi files are archived in ```datas/archives/midi```.
To make wav file of it, using the correct Midi instruments, use :

> python convert_midi_to_wav.py

A light UI has been made. Simply choose the file you want to convert in the menu and convert it. It will be saved in ```datas/archives/wav```.

## Misc infos

Match table between Midi notes and musical notes :

https://www.linuxrouen.fr/wp/wp-content/uploads/2019/01/midi_note-numero-nom.jpg

Generate HTML documentation from docstring :

> pydoc -w pieces_sonores
