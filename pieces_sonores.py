# coding=utf8
import numpy as np
import argparse
import imutils
import cv2
import os
import re
import fpt
import json
import color_tools
import pretty_midi as pm
import Tkinter as tk
import ttk

####

debug = False

# global variables
cap = None  # opencv capture element
cap_cam_index = 0  # opencv camera index to use for stream capture

# Repositories structure for the
datas_folder = "datas"  # main datas folder, everything will be in there
archive_folder = datas_folder + "/" + "archives"  # archives folder

# subfolders by datas types
images_sub_folder = "images"
midi_sub_folder = "midi"

# image extension
image_extension = ".jpg"

images_files_prefix = "capture_"
midi_files_prefix = "compo_"

archive_images_folder = archive_folder + "/" + images_sub_folder
archive_midi_folder = archive_folder + "/" + midi_sub_folder

images_process_folder = datas_folder + "/" + images_sub_folder
calibration_image_file = images_process_folder + "/" + "calibration.jpg"
current_capture_file = images_process_folder + "/" + "camera.jpg"

midi_process_folder = datas_folder + "/" + midi_sub_folder
current_midi_file = midi_process_folder + "/" + "current.mid"

colors_json_file_path = datas_folder + "/colors_config.json"

first_note = 48   # MIDI note number of lowest note
notes = 13        # number of notes
steps = 32        # number of time steps

tempo = 120
unit_duration = float(tempo) / (4 * tempo)

bounding_rectangle_to_coordinates = None
calibration_rect = None

# Spatial constants for 2D image analysis
# how much margin for acceptance of a vertically not-aligned piece?
vertical_margin = 0.20
horizontal_margin = 0.45
line_thickness = 0.05

# approxPolyDP factor
approxFactor = 0.02

# Base HSV ranges for color separation !Made by Yann!
blue_low_limit = (154, 165, 37)
blue_high_limit = (165, 255, 255)

green_low_limit = (85, 114, 0)
green_high_limit = (153, 255, 255)

red_low_limit = (35, 102, 0)
red_high_limit = (73, 255, 255)

hsv_low_limit = [red_low_limit, green_low_limit, blue_low_limit]
hsv_high_limit = [red_high_limit, green_high_limit, blue_high_limit]

# H values are shifted by +50, because red was on the 179/0 boundary without the shift
h_shift = 50


#######################################################
# calibration colors & json file preparation
#######################################################

# Object used to generate the json config file for color tracking
colors_ranges = {
    "blue":
        {
            "low": blue_low_limit,
            "high": blue_high_limit
        },
    "green":
        {
            "low": green_low_limit,
            "high": green_high_limit
        },
    "red":
        {
            "low": red_low_limit,
            "high": red_high_limit
        }
}

# the json file to open and update
colors_json_file = None

def prepare_colors_json():
    global colors_json_file, colors_ranges, hsv_low_limit, hsv_high_limit

    if not os.path.isfile(colors_json_file_path):
        print("json color file doesn't exist")
        colors_json_file = open(colors_json_file_path, "w")
        colors_ranges_json = json.dumps(colors_ranges)
        print(colors_ranges_json)
        colors_json_file.write(colors_ranges_json)
        colors_json_file.close()
    else:
        print("json color file found")
        colors_json_file = open(colors_json_file_path, "r")  # read write
        # file processing
        colors_ranges = json.loads(colors_json_file.read())
        colors_json_file.close()

    hsv_low_limit = [
        np.asarray(colors_ranges.get("red").get("low")),
        np.asarray(colors_ranges.get("green").get("low")),
        np.asarray(colors_ranges.get("blue").get("low"))
    ]
    hsv_high_limit = [
        np.asarray(colors_ranges.get("red").get("high")),
        np.asarray(colors_ranges.get("green").get("high")),
        np.asarray(colors_ranges.get("blue").get("high"))
    ]
    if debug:
        print("hsv_low_limit", hsv_low_limit, "hsv_high_limit", hsv_high_limit)


def update_colors_json():
    global colors_json_file, colors_ranges

    print("update_colors_json", colors_ranges)
    colors_json_file = open(colors_json_file_path, "w")
    colors_ranges_json = json.dumps(colors_ranges)
    print(colors_ranges_json)
    colors_json_file.write(colors_ranges_json)
    colors_json_file.close()

#######################################################
# folders archives related functions
#######################################################


def prepare_save():
    """Builds the necessary folders to auto organize
    files that will be archived at each use of the project
    returned values are int representing the number of files
    currently existing inside the datas folders
    :return: image_nb midi_nb
    :rtype: int int
    """
    global midi_nb

    # make missing directories if needed
    if not os.path.isdir(archive_folder):
        os.mkdir(archive_folder)

    if not os.path.isdir(archive_images_folder):
        os.mkdir(archive_images_folder)

    if not os.path.isdir(archive_midi_folder):
        os.mkdir(archive_midi_folder)

    # regex to check if string is a number
    regex = re.compile("\d+")

    def is_number(string):
        search = regex.search(string)
        result = -1
        if search is not None:
            result = int(search.group(0))
        return result

    image_nb = 0
    for f in os.listdir(archive_images_folder):
        # print("file :", f)
        nf = is_number(f)
        if nf == -1:
            break

        if nf > image_nb:
            image_nb = nf
        # a l'issue de la boucle, image_nb contient le numero de la plus
        # grande image presente dans le repertoire
        image_nb = image_nb + 1

    midi_nb = 0
    for f in os.listdir(archive_midi_folder):
        # print("file :", f)
        nf = is_number(f)
        if nf == -1:
            break

        if nf > midi_nb:
            midi_nb = nf
        # a l'issue de la boucle, midi_nb contient le is_number du plus
        # grand fichier midi dans le repertoire
        midi_nb = midi_nb + 1

    return (image_nb, midi_nb)


#######################################################
# midi related functions
#######################################################

def insert_note_containers(num, duree, n, list):
    list[num] += [(n, duree)]


def identify_note(image, x, y):
    u = image.shape[1]
    v = image.shape[0]

    pitch = y * notes / v
    step = (u - x) * steps / u - 1

    if debug:
        print("pitch: " + str(pitch) + ", step: " + str(step))

    return (pitch, step)


#######################################################
# opencv related functions
#######################################################

# Simple utility functions for image management


def open_video_capture(cam_index, width=1000, height=1000):
    global cap
    """open a camera stream"""
    cap = cv2.VideoCapture(cam_index)
    cap.set(cv2.CAP_PROP_FRAME_WIDTH, width)
    cap.set(cv2.CAP_PROP_FRAME_HEIGHT, height)
    if not cap.isOpened():
        cap.open(0)

    return cap


def close_video_capture(capture):
    global cap
    """close a camera stream"""
    if cap is not None:
        if cap.isOpened():
            cap.release()

    if capture is not None:
        if capture.isOpened():
            capture.release()


def open_image(path):
    image = cv2.imread(path)
    return image


def show_image(window_tile, image, width=None):
    """Display the defined image in a window"""
    img = None
    # if we want to resize the image
    if width != None:
        img = resizeWithAspectRatio(image, width)
    else:
        img = image

    cv2.imshow(window_tile, img)
    if cv2.waitKey(0):
        cv2.destroyAllWindows()


def resizeWithAspectRatio(image, width=None, height=None, inter=cv2.INTER_AREA):
    dim = None
    (h, w) = image.shape[:2]

    if width is None and height is None:
        return image
    if width is None:
        r = height / float(h)
        dim = (int(w * r), height)
    else:
        r = width / float(w)
        dim = (width, int(h * r))

    return cv2.resize(image, dim, interpolation=inter)


def debug_image(window_tile, image):
    global set_image_upside_down

    if set_image_upside_down:
        result = rotate_image_for_display(image, 180)
        show_image(window_tile, result, 800)
    else:
        show_image(window_tile, image, 800)


def rotate_image_for_display(image, angle):
    result = imutils.rotate(image, angle)
    return result


def capture_image(capture, name):
    """capture and save an image from a stream
    capture that is already opened
    example : capture_image(cap, "test.jpg")

    :param cap: the opencv capture object
    :param name: the file's string name WITH EXTENSION!
    :return: opencv frame
    """
    global cap
    if not cap:
        if not capture:
            cap = open_video_capture(0)
        else:
            cap = capture

    ret, frame = cap.read()
    cv2.imwrite(name, frame)
    close_video_capture(cap)
    cap = None
    return frame


def archive_image(image_number, frame):
    global debug
    url = archive_images_folder + "/" + images_files_prefix + str(image_number) + image_extension

    if debug:
        print("image ", image_number, url)

    cv2.imwrite(url, frame)
    image_number += 1
    return image_number


def test_cam(cam_index):
    """test a specific camera : show the video stream in a simple window
    to check if everything is OK
    """
    cap = cv2.VideoCapture(cam_index)
    while True:
        # Capture frame-by-frame
        ret, frame = cap.read()
        cv2.imshow("frame", frame)
        if cv2.waitKey(1) & 0xFF == ord("q"):
            break
    cap.release()
    cv2.destroyAllWindows()


# click count
clickNb = 0

def get_rbg_value_callback(event, x, y, flags, param):
    global clickNb, hsv_low_limit, hsv_high_limit

    image = param
    # checks mouse left button down condition
    if event == cv2.EVENT_LBUTTONDOWN:
        colorsB = image[y, x, 0]
        colorsG = image[y, x, 1]
        colorsR = image[y, x, 2]
        colors = (colorsR, colorsG, colorsB)
        hsv_color, low_limit, high_limit = color_tools.generate_values_for_rgb(colors, 50)

        print("HSV : ", hsv_color, low_limit, high_limit)

        if clickNb == 0:
            colors_ranges["red"]["low"] = list(low_limit)
            colors_ranges["red"]["high"] = list(high_limit)
            hsv_low_limit[clickNb] = low_limit
            hsv_high_limit[clickNb] = high_limit
        elif clickNb == 1:
            colors_ranges["green"]["low"] = list(low_limit)
            colors_ranges["green"]["high"] = list(high_limit)
            hsv_low_limit[clickNb] = low_limit
            hsv_high_limit[clickNb] = high_limit
        elif clickNb == 2:
            colors_ranges["blue"]["low"] = list(low_limit)
            colors_ranges["blue"]["high"] = list(high_limit)
            hsv_low_limit[clickNb] = low_limit
            hsv_high_limit[clickNb] = high_limit
            update_colors_json()

        print("click ", clickNb, colors_ranges)
        clickNb += 1
        clickNb %= 3


def get_rbg_from_image(path):
    image = open_image(path)
    # hsv_image = cv2.cvtColor(image, cv2.COLOR_BGR2HSV)
    cv2.namedWindow("get_rbg_value")
    cv2.setMouseCallback("get_rbg_value", get_rbg_value_callback, image)
    show_image("get_rbg_value", image)


#######################################################
# calibration
#######################################################

def compute_transform(src, dst):
    """
    A function that tries to correct the caputred image
    to get it more straight and orthogonal.
    This helps to avoid error on image analysis
    """
    srcTri = np.array(src).astype(np.float32)
    dstTri = np.array(dst).astype(np.float32)

    return cv2.getAffineTransform(dstTri, srcTri)


def new_calibration(input):
    global debug

    if debug:
        print("calibration input :", type(input), input)

    if isinstance(input, type(cv2.VideoCapture())):
        image = capture_image(input, calibration_image_file)
    elif isinstance(input, str):
        image = cv2.imread(calibration_image_file)
    else:
        raise Exception("wrong input type", type(input))

    rect = calibration(image)

    return rect.reshape(4, 2)


def info_peri_area(p, a):
    print("Perimeter: " + str(p) + "; area: " + str(a) + "100 * area / (perimeter)^2: " + str(100 * a / p * p))


def calibration(image):
    global debug

    color_index = 2  # calibration en bleu

    shapeMask = extract_color(image, color_index)

    if debug:
        debug_image("Calibration", shapeMask)

    cnts = cv2.findContours(shapeMask.copy(), cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_NONE)
    cnts = imutils.grab_contours(cnts)
#    print("\n\nI found {} shapes".format(len(cnts)))

    vertices = []    # we collect here all the vertices of the 4 pieces

    min_area = image.shape[0] * image.shape[1] / (notes * steps * 4)
    if debug:
        print("min_area" + str(min_area))

    for c in cnts:
        c = cv2.convexHull(c)
        (c_len, c_area) = peri_area(c)

        if 32 * c_area > c_len * c_len and c_area > min_area:
            # print(32*c_area)
            # print(">")
            # print(c_len*c_len)
            # print("----")
            if debug:
                info_peri_area(c_len, c_area)

            # estimation of the contours of the pieces used for calibration
            peri = cv2.arcLength(c, True)
            approx = cv2.approxPolyDP(c, approxFactor * peri, True)
            rect = cv2.minAreaRect(approx)

            # get the vertices from the rotated rectangle
            box = cv2.boxPoints(rect)  # cv2.boxPoints(rect) for OpenCV 3.x
            box = np.int0(box)
            cv2.drawContours(image, [box], 0, (0, 0, 255), 2)

            vertices.append(box)

    if debug:
        debug_image("Calibration", image)

    vlist = np.concatenate(vertices, 0)

    hull = cv2.convexHull(vlist)
    peri = cv2.arcLength(hull, True)
    approx = cv2.approxPolyDP(hull, approxFactor * peri, True)

    if len(approx) != 4:
        print("Calibration FAILED: ", len(approx), " pieces detectees")
        print("La calibration s'effectue en placant 4 pieces bleues rectangulaires de taille moyenne dans les 4 angles de la grille")
        return None

    return approx


#######################################################
# analysis and forms recognition
#######################################################


def shift_hue_value(h):
    return ((h + h_shift) % 180)


def shift_hue_image(img):
    for i in range(img.shape[0]):
        for j in range(img.shape[1]):
            img[i, j, 0] = shift_hue_value(img[i, j, 0])


def extract_color(image, color_index):
    '''
    extract pixels of a given color (color = 0, 1 or 2) as an 8-bit B&W image
    '''

    hsv_image = cv2.cvtColor(image, cv2.COLOR_BGR2HSV)
    # shift the hue values in the hsv image
    shift_hue_image(hsv_image)

    shapeMask = cv2.inRange(
        hsv_image, hsv_low_limit[color_index], hsv_high_limit[color_index])

    return shapeMask


def partition_setup():
    # Setup of prettyMIDI partition and instruments
    # 192 is the MIDI temporal resolution. Does it matter?
    partition = pm.PrettyMIDI(None, 192, tempo)

    pmi = [0, 0, 0, 0, 0, 0, 0, 0, 0]
    pmi[0] = pm.Instrument(0, False, "Marimba")
    pmi[1] = pm.Instrument(1, False, "Violoncelle")
    pmi[2] = pm.Instrument(2, False, "Trombonne")
    pmi[3] = pm.Instrument(3, False, "Marimba-Moyen")
    pmi[4] = pm.Instrument(4, False, "Violoncelle-Moyen")
    pmi[5] = pm.Instrument(5, False, "Trombonne-Moyen")
    pmi[6] = pm.Instrument(6, False, "Marimba-Long")
    pmi[7] = pm.Instrument(7, False, "Violoncelle-Long")
    pmi[8] = pm.Instrument(8, False, "Trombonne-Long")

    for i in range(0, 9):
        partition.instruments.append(pmi[i])

    return (partition, pmi)


def grid_width_height(image_width, image_height):
    grid_width = image_width / steps
    grid_height = image_height / notes
    reduced_grid_width = float(grid_width) * (1 - line_thickness)
    return (grid_width, grid_height, reduced_grid_width)


def peri_area(c):
    return (cv2.arcLength(c, True), cv2.contourArea(c))


def bounding_rectangle_to_coordinates_upright(image_width, image_height, rect):
    return rect


def bounding_rectangle_to_coordinates_upside_down(image_width, image_height, rect):
    return (image_width - rect[0] - rect[2], image_height - rect[1] - rect[3],
            rect[2], rect[3])


def admissible_rectangle(rect, grid_width, grid_height):
    (x, y, w, h) = rect
    y_r = float(h) / grid_height
    if (abs(y_r - 1)) > vertical_margin:
        if debug:
            print("Contour removed: height = " + str(h) + "; height/grid_height = " + str(y_r))
        return False
    x_r = float(w) / grid_width
    if (x_r < 1 - horizontal_margin or x_r > 4 + horizontal_margin):
        if debug:
            print("Contour removed: width = " + str(w) + "; width/grid_width = " + str(x_r))
        return False
    return True


def duration(rect, grid_width, grid_height):
    (x, y, w, h) = rect
    x_r = float(w) / grid_width
    for d in [1, 2, 4]:
        if (abs(x_r - d) < horizontal_margin):
            return d
    if debug:
        print("Contour removed: width = " + str(w) + "; width/grid_width = " + str(x_r))
    return None


def shape(img, rect, margin):
    (x0, y0, w, h) = rect
    x1 = x0 + w - 1
    y1 = y0 + h - 1
    crop1 = img[y0:y1, x0:x0 + margin]
    area1 = np.count_nonzero(crop1)
    crop2 = img[y0:y1, x1 - margin:x1]
    area2 = np.count_nonzero(crop2)
    area = area1 + area2
    print("Area: " + str(area))
    return area


def analyze_image(mat, midi_nb):
    global debug

    image = cv2.imread(current_capture_file)

    # 4-point transform to warp image: after the transform,
    #      grid and image boundaries coincide
    image = fpt.four_point_transform(image, calibration_rect)

    if debug:
        debug_image("corrected image", image)

    # create the pretyMIDI partition, together with instruments
    partition, pmi = partition_setup()

    # compute width and height of image and grid elements
    i_h, i_w, z = image.shape
    if debug:
        print("image sizes: width: ", i_w, "height", i_h)

    grid_width, grid_height, reduced_grid_width = grid_width_height(i_w, i_h)
    if debug:
        print("grid_width", grid_width, "grid_height", grid_height)

    shapes = [[]] * 3

    # image analysis, instrument by instrument
    for k in range(0, 3):
        # extract color mask
        shapeMask = extract_color(image, k)

        # compute contours
        cnts = cv2.findContours(
            shapeMask.copy(), cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_NONE)
        cnts = imutils.grab_contours(cnts)

        if debug:
            print("\n\nI found {} shapes".format(len(cnts)))

        if debug:
            debug_image("Mask", shapeMask)

        # analyze contours
        for c in cnts:
            i_rect = cv2.boundingRect(c)
            rect = bounding_rectangle_to_coordinates(i_w, i_h, i_rect)
            (x0, y0, w, h) = rect

            if not admissible_rectangle(rect, reduced_grid_width, grid_height):
                continue

            time = duration(rect, reduced_grid_width, grid_height)
            if time == None:
                continue

            s = shape(shapeMask, i_rect, 5)
            shapes[k].append(s)

            if debug:
                (p, a) = peri_area(c)
                info_peri_area(p, a)
                (i_x0, i_y0, u, v) = i_rect
                i_x1 = i_x0 + u
                i_y1 = i_y0 + v
                cv2.rectangle(image, (i_x0, i_y0), (i_x1, i_y1), (0, 0, 255), 1)

            step = (x0 + (grid_width / 2)) / grid_width
            pitch = (i_h - y0 + (grid_height / 2)) / grid_height

            # replace contour by its convex hull
            c = cv2.convexHull(c)

            # compute the track shift due to duration;
            # only values for 1, 2, 4 are meaningful
            inst_shift = [-1, 0, 3, -1, 6][time]

            pmi[k + inst_shift].notes.append(pm.Note(110, first_note + pitch, unit_duration * step, unit_duration * (step + time)))

            if debug:
                print("Note: track #: " + str(k + inst_shift) + "MIDI note #: "
                      + str(first_note + pitch) + "; step: " + str(step)
                      + "; duration: " + str(time))

    for k in range(3):
        print (shapes[k])
        shapes[k].sort()
        print (shapes[k])

    if debug:
        debug_image("notes", image)
        for inst in partition.instruments:
            for note in inst.notes:
                print(inst, "note :", note)

    partition.synthesize()
    partition.write(current_midi_file)
    partition.write(archive_midi_folder + "/midi" + str(midi_nb) + ".mid")


############
# events callback
############

def gui_calibrate_callback():
    global calibration_rect
    print ("gui_calibrate")
    calibration_rect = new_calibration(calibration_image_file)


def gui_get_rgb_value_callback():
    print ("gui_get_rgb_value_callback")
    get_rbg_from_image(calibration_image_file)
    # get_rbg_from_image(current_capture_file)


def gui_capture_calibration_image_callback():
    print ("gui_capture_calibration_image")
    frame = capture_image(cap, calibration_image_file)


def gui_capture_image_callback():
    global image_nb
    print ("gui_capture_image")
    frame = capture_image(cap, current_capture_file)
    image_nb = archive_image(image_nb, frame)


def gui_test_camera_callback():
    global test_camera_nb
    print("gui_test_camera", test_camera_nb)
    test_cam(test_camera_nb)


def gui_analyze_image_callback():
    global calibration_rect, midi_nb

    print("gui_analyze_image")
    analyze_image(calibration_rect, midi_nb)
    midi_nb = midi_nb + 1


def gui_play_midi_callback():
    print ("gui_play_midi")
    os.system("tools/timidity " + current_midi_file + " -a -c tools/timidity.cfg")
    # os.system("tools/timidity " + current_midi_file)


def gui_debug_callback():
    global debug
    debug = not debug
    print("debug = ", debug)


def gui_quit_callback():
    print ("gui_quit_callback")
    close_video_capture(cap)
    cv2.destroyAllWindows()
    # colors_json_file.close()  # close the json file
    print("Exiting")
    quit()


# Which camera to test ?
test_camera_nb = 0

def get_test_camera_nb_callback(event):
    global test_camera_nb
    test_camera_nb = event.widget.get()
    print("test_camera_nb", test_camera_nb)


def gui_set_image_upside_down_callback():
    global set_image_upside_down
    set_image_upside_down = not set_image_upside_down
    if set_image_upside_down:
        bounding_rectangle_to_coordinates = bounding_rectangle_to_coordinates_upside_down
    else:
        bounding_rectangle_to_coordinates = bounding_rectangle_to_coordinates_upright
    print("set_image_upside_down = ", set_image_upside_down)


################
# gui elements
################

def make_calibrate_ui(root):
    button = tk.Button(root,
                       text="Calibrate",
                       command=gui_calibrate_callback
                       )
    button.pack()


def make_get_rgb_value_from_calibration_image(root):
    button = tk.Button(root,
                       text="Pick R, G, B Value from image",
                       command=gui_get_rgb_value_callback
                       )
    button.pack()


def make_capture_calibration_ui(root):
    button = tk.Button(root,
                       text="Capture Calibration image",
                       command=gui_capture_calibration_image_callback
                       )
    button.pack()


def make_capture_image_ui(root):
    button = tk.Button(root,
                       text="Capture image",
                       command=gui_capture_image_callback
                       )
    button.pack()


def make_analyze_image_ui(root):
    button = tk.Button(root,
                       text="Analyze image",
                       command=gui_analyze_image_callback
                       )
    button.pack()


def make_play_midi_ui(root):
    button = tk.Button(root,
                       text="Play Midi file",
                       command=gui_play_midi_callback
                       )
    button.pack()


def make_quit_ui(root):
    button = tk.Button(root,
                       text="Quit",
                       command=gui_quit_callback
                       )
    button.pack()


def make_camera_test_ui(root):
    frame = tk.Frame(root)
    frame.pack()

    button_test_camera = tk.Button(
        frame, text="Test camera",
        command=gui_test_camera_callback
    )
    button_test_camera.pack(side=tk.LEFT)

    cam_list = []
    i = 0
    while i <= 10:
        cam_list.append(i)
        i += 1

    cam_menu = ttk.Combobox(frame, values=cam_list, width=3)
    cam_menu.current(0)
    cam_menu.bind("<<ComboboxSelected>>", get_test_camera_nb_callback)
    cam_menu.pack()


def make_debug_gui(root):
    global debug
    frame = tk.Frame(root)
    frame.pack()
    label = tk.Label(frame, text="Debug Mode")
    label.pack(side=tk.LEFT
               )
    button = tk.Checkbutton(frame, var=debug, command=gui_debug_callback)
    button.pack(side=tk.RIGHT)


def make_set_image_upside_down_ui(root):
    global set_image_upside_down
    frame = tk.Frame(root)
    frame.pack()
    label = tk.Label(frame, text="Make images upside down")
    label.pack(side=tk.LEFT
               )
    button = tk.Checkbutton(frame, var=set_image_upside_down,
                            command=gui_set_image_upside_down_callback)
    button.pack(side=tk.RIGHT)


image_nb = None
midi_nb = None
set_image_upside_down = None

def main():

    global debug, image_nb, midi_nb, calibration_rect
    global bounding_rectangle_to_coordinates

    # upside-down operation
    bounding_rectangle_to_coordinates =  \
        bounding_rectangle_to_coordinates_upside_down

    # prepare the archive folders
    image_nb, midi_nb = prepare_save()

    # prepare the color tracking values json config file
    prepare_colors_json()

    # initial calibration
    calibration_rect = new_calibration(calibration_image_file)

    # user interface set-up
    root = tk.Tk()
    root.geometry('300x350')

    make_camera_test_ui(root)

    sep = ttk.Separator(root, orient='horizontal')
    sep.pack(side='top', fill='x', pady=5)

    make_capture_calibration_ui(root)
    make_get_rgb_value_from_calibration_image(root)
    make_calibrate_ui(root)

    sep = ttk.Separator(root, orient='horizontal')
    sep.pack(side='top', fill='x', pady=5)

    make_capture_image_ui(root)
    make_analyze_image_ui(root)
    make_play_midi_ui(root)

    sep = ttk.Separator(root, orient='horizontal')
    sep.pack(side='top', fill='x', pady=5)

    make_debug_gui(root)
    make_set_image_upside_down_ui(root)

    sep = ttk.Separator(root, orient='horizontal')
    sep.pack(side='top', fill='x', pady=5)

    make_quit_ui(root)

    # start UI interaction
    root.mainloop()
    root.destroy()


if __name__ == "__main__":
    # execute only if run as a script
    main()
