# coding=utf8
import numpy as np
import cv2


def cv_rgb2hsv(rgb):

    nparray = np.uint8([[[rgb[0], rgb[1], rgb[2]]]])
    hsv_array = cv2.cvtColor(nparray, cv2.COLOR_RGB2HSV)
    hsv_array[0][0][0] = (hsv_array[0][0][0] + 50) % 180
    print("cv_rgb2hsv :", hsv_array)

    return hsv_array


def cv_hsv2rgb(hsv):

    nparray = np.uint8([[[hsv[0], hsv[1], hsv[2]]]])
    rgb_array = cv2.cvtColor(nparray, cv2.COLOR_HSV2RGB)
    print("cv_hsv2rgb :", rgb_array)

    return rgb_array


def generate_high_limit(hsv_color, threshold):
    array = hsv_color[0][0]
    h = (array[0] + 10) % 180
    s = array[1] + threshold * 2
    if s > 255:
        s = 255
    v = array[2] + threshold * 2.5
    if v > 255:
        v = 255

    return np.array([int(h), int(s), int(v)])


def generate_low_limit(hsv_color, threshold):

    array = hsv_color[0][0]
    h = (array[0] - 10) % 180
    s = array[1] - threshold
    if s < 0:
        s = 0
    v = array[2] - threshold
    if v < 0:
        v = 0

    return np.array([int(h), int(s), int(v)])


def generate_values_for_rgb(rgb_color, threshold):

    hsv_color = cv_rgb2hsv(rgb_color)
    low_limit = generate_low_limit(hsv_color, threshold)
    high_limit = generate_high_limit(hsv_color, threshold)

    return hsv_color, low_limit, high_limit
